import os
import unittest

from flask import Flask

from app import app
from app import db, models

def add(x,y):
    return x + y

class TestCase(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    # Post Data:

    # Test adding data to database
    def test_add_post_data(self):
        post = models.Post(title='Test', email='test@test.com', message='This is a test')
        db.session.add(post)
        db.session.commit()

        exists = models.Post.query.filter_by(title='Test').first()
        assert (exists is not None)

    # Test removing data from database
    def test_remove_post_data(self):
        models.Post.query.filter_by(title='Test').delete()
        db.session.commit()

        exists = models.Post.query.filter_by(title='Test').first()
        assert (exists is None)

    # User Data:

    # Test adding data to database
    def test_add_user_data(self):
        register = models.User(email='test@test.com', password='password')
        db.session.add(register)
        db.session.commit()

        exists = models.User.query.filter_by(email='test@test.com').first()   
        assert (exists is not None)

    # Test removing data from database
    def test_remove_user_data(self):
        models.User.query.filter_by(email='test@test.com').delete()
        db.session.commit()

        exists = models.User.query.filter_by(email='test@test.com').first()
        assert (exists is None)

if __name__ == '__main__':
    unittest.main()