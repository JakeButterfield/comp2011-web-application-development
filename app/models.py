from app import db

def defaultIcon():
	return "https://i.imgur.com/F3Ny6xJ.png"

class User(db.Model):
	__tablename__ = "user"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	email = db.Column(db.String(500), nullable=False)
	password = db.Column(db.String(500), nullable=False)
	icon = db.Column(db.String(500), nullable=False, default=defaultIcon)
	posts = db.relationship('Post', back_populates="userrel")

class Post(db.Model):
	__tablename__ = "post"
	id = db.Column(db.Integer, primary_key=True, autoincrement=True)
	title = db.Column(db.String(500), nullable=False)
	user = db.Column(db.Integer, db.ForeignKey('user.id'))
	message = db.Column(db.String(500), nullable=False)
	userrel = db.relationship("User", back_populates="posts")