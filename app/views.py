import string
import random

from flask import render_template, flash, redirect, url_for, session, request, make_response
from app import app
from .forms import LoginForm, SignupForm, PostForm, PasswordChangeForm, IconChangeForm

from datetime import datetime

from app import db, models

severityList = ['TEST', 'WARNING', 'UPDATE', 'SEVERE']
def appLog( severe, log ):
	file = open("log.txt", "a")
	file.write("{1} -- {0} | {2}\n".format(severityList[severe], datetime.now().strftime("%Y-%m-%d %H:%M"), log))
	file.close()

@app.route('/', methods=['GET', 'POST'])
def index():

	if 'email' in session:
		return redirect(url_for('home'))

	# Login Form

	login_form = LoginForm()
	if login_form.validate_on_submit() and login_form.loginSubmit.data:

		postEmail = login_form.email.data
		postPass = login_form.password.data
		
		exists = models.User.query.filter_by(email=postEmail).first()

		if not (exists):
			flash("Email not found")

		else:
			if not (postPass == exists.password):
				flash("Password Incorrect")
				appLog(3, "{0} attempted to login with an incorrect password".format(postEmail))
			else:
				# Do session things here
				session['email'] = postEmail
				session['userid'] = exists.id

				appLog(1, "{0} logged in".format(postEmail))

				return redirect(url_for('home'))

	# Register Form

	register_form = SignupForm()
	if register_form.validate_on_submit() and register_form.registerSubmit.data:

		exists = models.User.query.filter_by(email=register_form.email.data).first()
		
		if not (register_form.password.data == register_form.confirmPassword.data):
			flash("Your passwords did not match!")

		elif ( exists ):
			flash("That email is already registered!")

		else:
			default_icon = 'https://i.imgur.com/F3Ny6xJ.png'
			register = models.User(email=register_form.email.data, password=register_form.password.data, icon=default_icon)
			db.session.add(register)
			db.session.commit()

			appLog(1, "{0} has registered".format(postEmail))
			return redirect(url_for('home'))

	# Render View

	return render_template('index.html',
						   title='Login/Signup',
						   login_form=login_form,
						   register_form=register_form)

@app.route('/home', methods=['GET', 'POST'])
def home():
	if not 'email' in session:
		return redirect( url_for( 'index' ) )

	posts = {}
	postAmount = models.Post.query.count()
	for queryPost in models.Post.query.all():
		user = models.User.query.filter_by(id=queryPost.user).first()
		newPost = []
		newPost.append( queryPost.title )
		newPost.append( user.email )
		newPost.append( queryPost.message )
		newPost.append( user.icon )
		posts[queryPost.id] = newPost

	return render_template('home.html',
						   title='Home',
						   posts=posts,
						   postAmount=postAmount)

@app.route('/newpost', methods=['GET', 'POST'])
def newpost():
	if not 'email' in session:
		return redirect(url_for('index'))

	email = session['email']
	userid = session['userid']

	form = PostForm()
	if form.validate_on_submit():
		post = models.Post(title=form.title.data, user=userid, message=form.post.data)
		db.session.add(post)
		db.session.commit()

		appLog(2, "{0} has created a new post titled {1}".format(email, form.title.data))

		return redirect( url_for( 'home' ) )

	return render_template('newpost.html',
						   title='New Post',
						   form=form)

@app.route('/profile', methods=['GET', 'POST'])
def profile():
	if not 'email' in session:
		return redirect(url_for('index'))

	# Login Form

	password_form = PasswordChangeForm()
	if password_form.validate_on_submit() and password_form.submitPass.data:

		oldPass = password_form.oldPass.data
		newPass = password_form.newPass.data
		confirmPass = password_form.confirmPass.data
		
		exists = models.User.query.filter_by(id=session['userid']).first()

		if not (exists):
			flash("User not found")

		else:
			if not (oldPass == exists.password):
				flash("Old Password Incorrect")
			else:
				if not (newPass == confirmPass):
					flash("New passwords do not match")
				else:
					updatePass = models.User.query.filter_by(id=session['userid']).update(dict(password=newPass))
					db.session.commit()
					appLog(2, "{0} has changed their password".format(exists.email))
					flash("Password changed successfully!")

	icon_form = IconChangeForm()
	if icon_form.validate_on_submit() and icon_form.submitIcon.data:

		newIcon = icon_form.newIcon.data
		iconExtension = newIcon[-4:]

		if not (iconExtension == ".png"):
			flash("Your chosen icon must be a .png link")
		else:
			updatePass = models.User.query.filter_by(id=session['userid']).update(dict(icon=newIcon))
			db.session.commit()
			appLog(2,"{0} has updated their email".format(models.User.query.filter_by(id=session['userid']).email))
			flash("Icon changed successfully!")			

	user = models.User.query.filter_by(id=session['userid']).first()
	icon = user.icon

	return render_template('profile.html',
						   title='Profile',
						   icon=icon,
						   icon_form=icon_form,
						   password_form=password_form)

@app.route('/logout')
def logout():
	appLog(2,"{0} has logged out".format(session['email']))
	session.pop('email', None)
	session.pop('userid', None)
	return redirect( url_for( 'index' ) )