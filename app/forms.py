from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import DataRequired, Email

from .models import User

class LoginForm(Form):
    email = StringField('Email', validators=[DataRequired(), Email()], render_kw={"placeholder": "Email"})
    password = PasswordField('Password', validators=[DataRequired()], render_kw={"placeholder": "Password"})
    loginSubmit = SubmitField('Login')

class SignupForm(Form):
    email = StringField('Email', validators=[DataRequired(), Email()], render_kw={"placeholder": "Email"})
    password = PasswordField('Password', validators=[DataRequired()],render_kw={"placeholder": "Password"})
    confirmPassword = PasswordField('ConfirmPassword', validators=[DataRequired()],render_kw={"placeholder": "Confirm Password"})
    consent = BooleanField('I consent to you storing and processing my email and safely securing your password', validators=[DataRequired()])
    registerSubmit = SubmitField('Signup')

class PostForm(Form):
    title = StringField('Title', validators=[DataRequired()], render_kw={"placeholder": "Title"})
    post = StringField('Post', validators=[DataRequired()], render_kw={"placeholder": "Post Content"})
    postSubmit = SubmitField('Post')

class PasswordChangeForm(Form):
    oldPass = PasswordField('Password', validators=[DataRequired()], render_kw={"placeholder": "Old Password"})
    newPass = PasswordField('Password', validators=[DataRequired()], render_kw={"placeholder": "New Password"})
    confirmPass = PasswordField('Password', validators=[DataRequired()], render_kw={"placeholder": "Confirm New Password"})
    submitPass = SubmitField('Change Password')

class IconChangeForm(Form):
    newIcon = StringField('Icon', validators=[DataRequired()], render_kw={"placeholder": "New Icon (Link to .png)"})
    submitIcon = SubmitField('Change Icon')